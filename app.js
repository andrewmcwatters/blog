/**
 * @license andrewmcwatters.com
 * (c) 2015 Andrew McWatters.
 */
'use strict';

var ghost      = require('ghost');
var path       = require('path');
var express    = require('express');
var app        = express();
module.exports = app;

ghost({
  config: path.join(__dirname, 'config.js')
}).then(function(ghostServer) {
  app.use(ghostServer.config.paths.subdir, ghostServer.rootApp);

  ghostServer.start(app);
});
